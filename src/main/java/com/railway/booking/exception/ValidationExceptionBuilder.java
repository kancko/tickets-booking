package com.railway.booking.exception;

import java.util.HashMap;
import java.util.Map;

import static com.railway.booking.exception.ValidationException.ValidationEnum;

public class ValidationExceptionBuilder {

    private Map<ValidationEnum, String> errors = new HashMap<>();

    public ValidationExceptionBuilder put(ValidationEnum key, String value) {
        errors.put(key, value);
        return this;
    }

    public void throwIfErrorExists() {
        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
