package com.railway.booking.exception;

import java.util.Map;

public class ValidationException extends RuntimeException {

    private Map<ValidationEnum, String> errors;

    ValidationException(Map<ValidationEnum, String> errors) {
        this.errors = errors;
    }

    public static ValidationExceptionBuilder builder() {
        return new ValidationExceptionBuilder();
    }

    public Map<ValidationEnum, String> getErrors() {
        return errors;
    }

    public static enum ValidationEnum {
        BAD_CREDENTIAL,
        DELETE_ERROR,
        USERNAME_EXISTS,
        PASSWORDS_DO_NOT_MATCH
    }
}
