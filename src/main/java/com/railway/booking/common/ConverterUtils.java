package com.railway.booking.common;

import com.railway.booking.entity.User;
import com.railway.booking.exception.ValidationException;

import javax.servlet.http.HttpServletRequest;

import static com.railway.booking.entity.User.Role.CONSUMER;
import static com.railway.booking.exception.ValidationException.ValidationEnum.PASSWORDS_DO_NOT_MATCH;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

public class ConverterUtils {
    public static User convertRequestToUser(HttpServletRequest request) {
        String username = request.getParameter("username");
        String fullname = request.getParameter("fullname");
        String password = request.getParameter("password");
        String rePassword = request.getParameter("rePassword");
        String passportSeries = request.getParameter("passportSeries");
        if (!password.equals(rePassword)) {
            ValidationException.builder().put(PASSWORDS_DO_NOT_MATCH, "Password don't match").throwIfErrorExists();
        }
        return User.builder()
                .username(username)
                .password(sha256Hex(password))
                .fullName(fullname)
                .isActive(true)
                .passportSeries(passportSeries)
                .role(CONSUMER)
                .build();
    }
}
