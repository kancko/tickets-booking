package com.railway.booking.config;

import com.railway.booking.repository.*;
import com.railway.booking.repository.impl.*;
import com.railway.booking.service.*;
import com.railway.booking.service.impl.*;
import com.railway.booking.transaction.TransactionManager;
import com.railway.booking.transaction.impl.TransactionManagerImpl;
import lombok.extern.slf4j.Slf4j;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

@Slf4j
@WebListener
public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.debug("context init start");
        ServletContext context = servletContextEvent.getServletContext();
        TransactionManager transactionManager = initTransactionManager();
        initServices(context, transactionManager);
        log.debug("context init finish");
    }

    private void initServices(ServletContext context, TransactionManager transactionManager) {
        UserRepository userRepository = new UserRepositoryImpl();
        TrainRepository trainRepository = new TrainRepositoryImpl();
        TrainCarRepository trainCarRepository = new TrainCarRepositoryImpl();
        TimetableRepository timetableRepository = new TimetableRepositoryImpl();
        TicketRepository ticketRepository = new TicketRepositoryImpl();
        CityRepository cityRepository = new CityRepositoryImpl();

        CityService cityService = new CityServiceImpl(cityRepository, transactionManager);
        UserService userService = new UserServiceImpl(transactionManager, userRepository);
        TrainCarService trainCarService = new TrainCarServiceImpl(trainCarRepository, transactionManager);
        TimetableService timetableService = new TimetableServiceImpl(timetableRepository, transactionManager);
        TicketService ticketService = new TicketServiceImpl(transactionManager, ticketRepository, trainCarService);
        TrainService trainService = new TrainServiceImpl(trainCarService,trainRepository, timetableService, transactionManager);

        context.setAttribute(UserService.class.toString(), userService);
        context.setAttribute(TrainService.class.toString(), trainService);
        context.setAttribute(TrainCarService.class.toString(), trainCarService);
        context.setAttribute(TimetableService.class.toString(), timetableService);
        context.setAttribute(TicketService.class.toString(), ticketService);
        context.setAttribute(CityService.class.toString(), cityService);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.debug("context destroyed");
    }

    private TransactionManager initTransactionManager() {
        try {
            DataSource dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/test");
            return new TransactionManagerImpl(dataSource);
        } catch (NamingException e) {
            throw new RuntimeException("data source cant init");
        }
    }
}
