package com.railway.booking.constant;

public enum Action {
    SAVE, DELETE
}
