package com.railway.booking.constant;

public class JspConstants {

    public static final String SIGN_UP = "/WEB-INF/jsp/sign-up.jsp";
    public static final String ADD_CITY_PAGE = "/WEB-INF/jsp/city-add";
    public static final String TRAINS_PAGE = "/WEB-INF/jsp/trains.jsp";
    public static final String USER_HOME = "/WEB-INF/jsp/user-home.jsp";
    public static final String USER_LOGIN_JSP = "/WEB-INF/jsp/login.jsp";
    public static final String ADMIN_HOME = "/WEB-INF/jsp/admin-home.jsp";
    public static final String MANAGER_HOME = "/WEB-INF/jsp/manager-home.jsp";
    public static final String ADD_MANAGER_PAGE = "/WEB-INF/jsp/manager-add.jsp";
    public static final String TRAINS_DETAILS = "/WEB-INF/jsp/train-details.jsp";
    public static final String TRAINS_MANAGE_PAGE = "/WEB-INF/jsp/manage-trains.jsp";
    public static final String BOOKING_TICKET_PAGE = "/WEB-INF/jsp/booking-ticket.jsp";
    public static final String TRAINS_DETAILS_MANAGE_PAGE = "/WEB-INF/jsp/manage-train-details.jsp";
}
