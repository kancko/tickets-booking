package com.railway.booking.repository;

import com.railway.booking.entity.Timetable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TimetableRepository extends EntityRepository<Timetable> {

    List<Timetable> getAllByTrainId(Connection connection, long trainId) throws SQLException;

    List<Timetable> getAll(Connection connection) throws SQLException;
}
