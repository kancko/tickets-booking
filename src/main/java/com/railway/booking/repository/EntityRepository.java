package com.railway.booking.repository;

import java.sql.Connection;
import java.sql.SQLException;

public interface EntityRepository<T> {

    T getById(Connection connection, long id) throws SQLException;

    long save(Connection connection, T entity) throws SQLException;

    T delete(Connection connection, long id) throws SQLException;

    T update(Connection connection, T entity) throws SQLException;
}
