package com.railway.booking.repository;

import com.railway.booking.entity.City;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface CityRepository extends EntityRepository<City> {

    List<City> getCities(Connection connection) throws SQLException;
}
