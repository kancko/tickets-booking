package com.railway.booking.repository;

import com.railway.booking.entity.Ticket;
import com.railway.booking.entity.TrainCar;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TrainCarRepository extends EntityRepository<TrainCar> {

    List<TrainCar> getAllByTrainId(Connection connection, long trainId) throws SQLException;

    List<TrainCar> getAll(Connection connection) throws SQLException;

    List<Integer> getBookedSeats(Connection connection, long trainId) throws SQLException;

    long saveTrainCarUser(Connection connection, Ticket ticket) throws SQLException;
}
