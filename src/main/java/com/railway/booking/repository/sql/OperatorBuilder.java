package com.railway.booking.repository.sql;

import java.util.Set;

public interface OperatorBuilder extends LogicalOperatorBuilder {
    LogicalOperatorBuilder equal(String fieldName, String value);

    LogicalOperatorBuilder like(String fieldName, String value);

    LogicalOperatorBuilder lessThan(String fieldName, String value);

    LogicalOperatorBuilder moreThan(String fieldName, String value);

    LogicalOperatorBuilder in(String fieldName, Set<?> set);
}
