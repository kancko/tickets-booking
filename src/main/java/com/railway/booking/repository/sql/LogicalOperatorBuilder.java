package com.railway.booking.repository.sql;

public interface LogicalOperatorBuilder extends OrderBuilder {
    OperatorBuilder and();

    OperatorBuilder or();

    OperatorBuilder not();
}
