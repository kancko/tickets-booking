package com.railway.booking.repository.sql;


public interface OrderBuilder extends LimitBuilder, PreparedStatementBuilder {

    LimitBuilder orderBy(String fieldName, boolean ascending);

    PreparedStatementBuilder groupBy(String filedName);

}
