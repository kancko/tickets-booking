package com.railway.booking.repository.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface SelectBuilder {

    OperatorBuilder where();

    PreparedStatement buildPrepareStatement(Connection connection) throws SQLException;

}
