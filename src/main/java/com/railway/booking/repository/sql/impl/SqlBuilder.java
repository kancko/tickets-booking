package com.railway.booking.repository.sql.impl;

import com.railway.booking.repository.sql.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SqlBuilder implements SelectBuilder, WhereBuilder {
    private static final Logger LOGGER = Logger.getLogger(SqlBuilder.class);
    private StringBuilder query = new StringBuilder();
    private List<Object> args = new ArrayList<>();

    public static SelectBuilder select(String selectQuery) {
        return new SqlBuilder(selectQuery);
    }

    private SqlBuilder(String selectQuery) {
        query.append(selectQuery);
    }

    @Override
    public OperatorBuilder where() {
        query.append(" WHERE ");
        return this;
    }

    @Override
    public WhereBuilder equal(String fieldName, String value) {
        if (value.isEmpty()) {
            query.append(fieldName).append(' ');
            return this;
        }
        query.append(fieldName).append(" = ").append("? ");
        args.add(value);
        return this;
    }

    @Override
    public WhereBuilder like(String fieldName, String value) {
        query.append(fieldName).append(" like (?) ");
        args.add('%' + value + '%');
        return this;
    }

    @Override
    public LogicalOperatorBuilder lessThan(String fieldName, String value) {
        if (value.isEmpty()) {
            query.append(fieldName).append(' ');
            return this;
        }
        query.append(fieldName).append(" <= ").append("? ");
        args.add(value);
        return this;
    }

    @Override
    public LogicalOperatorBuilder moreThan(String fieldName, String value) {
        if (value.isEmpty()) {
            query.append(fieldName).append(' ');
            return this;
        }
        query.append(fieldName).append(" >= ").append("? ");
        args.add(value);
        return this;
    }

    @Override
    public LogicalOperatorBuilder in(String fieldName, Set<?> set) {
        if (!set.isEmpty()) {
            query.append(fieldName).append(" IN ").append('(');
            boolean comma = false;
            for (Object o : set) {
                if (comma) {
                    query.append(',');
                } else {
                    comma = true;
                }
                query.append('?');
                args.add(o);
            }
            query.append(')');
        }
        return this;
    }

    @Override
    public PreparedStatementBuilder limit(int offset, int size) {
        query.append(" LIMIT ").append(offset).append(',').append(size);
        return this;
    }

    @Override
    public OrderBuilder orderBy(String fieldName, boolean ascending) {
        query.append(" ORDER BY ").append(fieldName).append(' ').append(ascending ? "ASC" : "DESC");
        return this;
    }

    @Override
    public PreparedStatementBuilder groupBy(String filedName) {
        query.append(" GROUP BY ").append(filedName);
        return this;
    }

    @Override
    public OperatorBuilder and() {
        query.append(" AND ");
        return this;
    }

    @Override
    public OperatorBuilder or() {
        query.append(" OR ");
        return this;
    }

    @Override
    public OperatorBuilder not() {
        query.append(" NOT ");
        return this;
    }

    @Override
    public PreparedStatement buildPrepareStatement(Connection connection) throws SQLException {
        String sql = query.toString();
        PreparedStatement statement = connection.prepareStatement(sql);
        LOGGER.debug("LOG SelectBuilder: " + sql);

        int i = 1;
        for (Object arg : args) {
            statement.setObject(i++, arg);
            sql = sql.replaceFirst("\\?", arg.toString());
        }
        LOGGER.debug("LOG SelectBuilder: " + sql);
        return statement;
    }
}
