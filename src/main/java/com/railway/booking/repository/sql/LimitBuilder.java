package com.railway.booking.repository.sql;

public interface LimitBuilder extends PreparedStatementBuilder {

    PreparedStatementBuilder limit(int offset, int size);

}
