package com.railway.booking.repository;

import com.railway.booking.entity.Ticket;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TicketRepository extends EntityRepository<Ticket> {

    List<Ticket> getAll(Connection connection, long userId) throws SQLException;
}
