package com.railway.booking.repository;

import com.railway.booking.entity.Train;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TrainRepository extends EntityRepository<Train> {

    List<Train> getTrains(Connection connection,
                          String startDate, String endDate,
                          String startCity, String endCity) throws SQLException;
}
