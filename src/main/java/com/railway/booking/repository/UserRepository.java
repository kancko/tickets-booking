package com.railway.booking.repository;

import com.railway.booking.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository extends EntityRepository<User> {

    User getUserByUsernameAndPassword(Connection connection, String username, String password) throws SQLException;

    boolean existsByUserName(Connection connection, String username) throws SQLException;

    List<User> getAll(Connection connection) throws SQLException;

    Long block(Connection connection, long userId, boolean active) throws SQLException;
}
