package com.railway.booking.repository.impl;

import com.railway.booking.entity.User;
import com.railway.booking.repository.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    private static final String SELECT_USER_BY_ID = "select * from users where id=?";
    private static final String INSERT_USER = "insert into users (username, password, full_name, passport_series, role, is_active) values(?,?,?,?,?,?)";
    private static final String DELETE_USER = "delete from users where id=?";
    private static final String UPDATE_USER = "update users set username=?, password=?, full_name=?, passport_series=? where id=?";
    private static final String EXISTS_USER_BY_USERNAME = "select * from users where username=?";
    private static final String SELECT_USER_BY_USERNAME_AND_PASSWORD = "select * from users where username=? and password=?";
    private static final String SELECT_ALL_USERS = "select * from users";
    private static final String BLOCK_USER = "update users set is_active=? where id=?";

    @Override
    public User getUserByUsernameAndPassword(Connection connection, String username, String password) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_USERNAME_AND_PASSWORD);
        statement.setString(1, username);
        statement.setString(2, password);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToUser(resultSet) : null;
    }

    @Override
    public List<User> getAll(Connection connection) throws SQLException {
        List<User> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_USERS);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToUser(resultSet));
        }
        return list;
    }

    @Override
    public boolean existsByUserName(Connection connection, String username) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXISTS_USER_BY_USERNAME);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public User getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToUser(resultSet) : null;
    }

    @Override
    public long save(Connection connection, User entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public User delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_USER);
        statement.setLong(1, id);
        statement.executeUpdate();
        return User.builder().id(id).build();
    }

    @Override
    public User update(Connection connection, User entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_USER);
        setAttributeForUpdate(entity, statement);
        statement.executeUpdate();
        return entity;
    }

    @Override
    public Long block(Connection connection, long userId, boolean active) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(BLOCK_USER);
        statement.setBoolean(1, active);
        statement.setLong(2, userId);
        statement.executeUpdate();
        return userId;
    }

    private User convertResultSetToUser(ResultSet resultSet) throws SQLException {
        return User.builder()
                .id(resultSet.getLong("id"))
                .passportSeries(resultSet.getString("passport_series"))
                .password(resultSet.getString("password"))
                .username(resultSet.getString("username"))
                .fullName(resultSet.getString("full_name"))
                .isActive(resultSet.getBoolean("is_active"))
                .role(User.Role.valueOf(resultSet.getString("role")))
                .build();
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForUpdate(User user, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setString(++count, user.getUsername());
        statement.setString(++count, user.getPassword());
        statement.setString(++count, user.getFullName());
        statement.setString(++count, user.getPassportSeries());
        statement.setLong(++count, user.getId());
    }

    private void setAttributeForSave(User user, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setString(++count, user.getUsername());
        statement.setString(++count, user.getPassword());
        statement.setString(++count, user.getFullName());
        statement.setString(++count, user.getPassportSeries());
        statement.setString(++count, user.getRole().name());
        statement.setBoolean(++count, user.isActive());
    }
}
