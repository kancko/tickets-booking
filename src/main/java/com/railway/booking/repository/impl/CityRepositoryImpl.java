package com.railway.booking.repository.impl;

import com.railway.booking.entity.City;
import com.railway.booking.repository.CityRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CityRepositoryImpl implements CityRepository {
    private static final String SELECT_ALL_CITIES = "select * from cities";
    private static final String SELECT_CITY_BY_ID = "select * from cities where id = ?";
    private static final String INSERT_CITY = "insert into cities (title) values('?')";
    private static final String DELETE_CITY = "delete from cities where id=?";
    private static final String UPDATE_CITY = "update cities set title=? where id=?";

    @Override
    public List<City> getCities(Connection connection) throws SQLException {
        List<City> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_CITIES);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToCity(resultSet));
        }
        return list;
    }

    @Override
    public City getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_CITY_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToCity(resultSet) : null;
    }

    @Override
    public long save(Connection connection, City entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_CITY, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public City delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_CITY);
        statement.setLong(1, id);
        statement.executeUpdate();
        return City.builder().id(id).build();
    }

    @Override
    public City update(Connection connection, City entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_CITY);
        setAttributeForUpdate(entity, statement);
        statement.executeUpdate();
        return entity;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForUpdate(City city, PreparedStatement statement) throws SQLException {
        statement.setString(1, city.getTitle());
        statement.setLong(2, city.getId());
    }

    private void setAttributeForSave(City city, PreparedStatement statement) throws SQLException {
        statement.setLong(1, city.getId());
    }

    private City convertResultSetToCity(ResultSet resultSet) throws SQLException {
        return City.builder()
                .id(resultSet.getLong("id"))
                .title(resultSet.getString("title"))
                .build();
    }
}
