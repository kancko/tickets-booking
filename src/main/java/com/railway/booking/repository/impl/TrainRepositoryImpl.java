package com.railway.booking.repository.impl;

import com.railway.booking.entity.City;
import com.railway.booking.entity.Train;
import com.railway.booking.repository.TrainRepository;
import com.railway.booking.repository.sql.impl.SqlBuilder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrainRepositoryImpl implements TrainRepository {

    private static final String BASE_TRAIN_SELECT = "select t.id t_id, t.train_number t_train_number, t.cost t_cost, " +
            "cs.id cs_id, cs.title cs_title, ce.id ce_id, ce.title ce_title," +
            "tm.arrival_time tm_arrival_time, tm.departure_time tm_departure_time " +
            "from trains t " +
            "join cities cs on cs.id = t.start_city " +
            "join cities ce on ce.id = t.end_city " +
            "left join timetable tm on tm.train_id = t.id";
    private static final String INSERT_TRAIN = "insert into trains (train_number, cost, start_city, end_city) values(?,?,?,?)";
    private static final String DELETE_TRAIN = "delete from trains where id = ?";
    private static final String UPDATE_TRAIN = "update users set train_number=?, cost=?, start_city=?, end_city=? where id=?";

    @Override
    public List<Train> getTrains(Connection connection, String startDate, String endDate, String startCity, String endCity) throws SQLException {
        List<Train> list = new ArrayList<>();
        PreparedStatement statement = SqlBuilder
                .select(BASE_TRAIN_SELECT)
                .where()
                .equal("t.start_city", String.valueOf(startCity))
                .and()
                .equal("t.end_city", String.valueOf(endCity))
                .and()
                .moreThan("tm.arrival_time", startDate)
                .and()
                .lessThan("tm.departure_time", endDate)
                .groupBy("t_id")
                .buildPrepareStatement(connection);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToTrain(resultSet));
        }
        return list;
    }

    @Override
    public Train getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = SqlBuilder
                .select(BASE_TRAIN_SELECT)
                .where()
                .equal("t.id", String.valueOf(id))
                .buildPrepareStatement(connection);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToTrain(resultSet) : null;
    }

    @Override
    public long save(Connection connection, Train entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TRAIN, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public Train delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_TRAIN);
        statement.setLong(1, id);
        statement.executeUpdate();
        return Train.builder().id(id).build();
    }

    @Override
    public Train update(Connection connection, Train entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TRAIN);
        setAttributeForUpdate(entity, statement);
        statement.executeUpdate();
        return entity;
    }

    private void setAttributeForUpdate(Train train, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setString(++count, train.getTrainNumber());
        statement.setLong(++count, train.getCost());
        statement.setLong(++count, train.getStartCity().getId());
        statement.setLong(++count, train.getEndCity().getId());
        statement.setLong(++count, train.getId());
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForSave(Train train, PreparedStatement statement) throws SQLException {
        int k = 0;
        statement.setString(++k, train.getTrainNumber());
        statement.setLong(++k, train.getCost());
        statement.setLong(++k, train.getStartCity().getId());
        statement.setLong(++k, train.getEndCity().getId());
    }

    private City getCityFromResultSet(ResultSet resultSet, String prefix) throws SQLException {
        return City.builder()
                .id(resultSet.getLong(prefix + "_id"))
                .title(resultSet.getString(prefix + "_title"))
                .build();
    }

    private Train convertResultSetToTrain(ResultSet resultSet) throws SQLException {
        return Train.builder()
                .id(resultSet.getLong("t_id"))
                .trainNumber(resultSet.getString("t_train_number"))
                .cost(resultSet.getLong("t_cost"))
                .startCity(getCityFromResultSet(resultSet, "cs"))
                .endCity(getCityFromResultSet(resultSet, "ce"))
                .build();
    }
}
