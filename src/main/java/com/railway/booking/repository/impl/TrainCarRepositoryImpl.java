package com.railway.booking.repository.impl;

import com.railway.booking.entity.Ticket;
import com.railway.booking.entity.TrainCar;
import com.railway.booking.repository.TrainCarRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;

public class TrainCarRepositoryImpl implements TrainCarRepository {

    private static final String SELECT_ALL_TRAINCARS_BY_TRAIN_ID = "select * from traincars where train_id = ?";
    private static final String SELECT_TRAINCAR_BY_ID = "select * from traincars where id = ?";
    private static final String INSERT_TRAINCAR = "insert into traincars (num, type, places_begins, places_ends, train_id) values(?,?,?,?,?)";
    private static final String INSERT_TRAINCAR_USER = "insert into traincars_users (traincar_id, user_id, booked_seat, timetable_id) values (?,?,?,?)";
    private static final String DELETE_TRAINCAR = "delete from traincars where id=?";
    private static final String UPDATE_TRAINCAR = "update traincars set num=?, type=?, places_begins=?, places_ends=? where id=?";
    private static final String SELECT_ALL = "select * from traincars";
    private static final String SELECT_BOOKED_SEATS_TRAINCARS_BY_TRAIN_ID = "select traincars_users.booked_seat seat from traincars_users where traincar_id in(select traincars.id from traincars where train_id = ?)";

    @Override
    public TrainCar getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_TRAINCAR_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToTrainCar(resultSet) : null;
    }

    @Override
    public long save(Connection connection, TrainCar entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TRAINCAR, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public long saveTrainCarUser(Connection connection, Ticket ticket) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TRAINCAR_USER, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(ticket, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public TrainCar delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_TRAINCAR);
        statement.setLong(1, id);
        statement.executeUpdate();
        return TrainCar.builder().id(id).build();
    }

    @Override
    public TrainCar update(Connection connection, TrainCar entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TRAINCAR);
        setAttributeForUpdate(entity, statement);
        statement.executeUpdate();
        return entity;
    }

    @Override
    public List<TrainCar> getAllByTrainId(Connection connection, long trainId) throws SQLException {
        List<TrainCar> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_TRAINCARS_BY_TRAIN_ID);
        statement.setLong(1, trainId);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToTrainCar(resultSet));
        }
        return list;
    }

    @Override
    public List<Integer> getBookedSeats(Connection connection, long trainId) throws SQLException {
        List<Integer> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_BOOKED_SEATS_TRAINCARS_BY_TRAIN_ID);
        statement.setLong(1, trainId);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(resultSet.getInt("seat"));
        }
        return list;
    }

    @Override
    public List<TrainCar> getAll(Connection connection) throws SQLException {
        List<TrainCar> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        while (resultSet.next()) {
            list.add(convertResultSetToTrainCar(resultSet));
        }
        return list;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForUpdate(TrainCar trainCar, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setLong(++count, trainCar.getNum());
        statement.setString(++count, valueOf(trainCar.getType()));
        statement.setLong(++count, trainCar.getPlacesBegins());
        statement.setLong(++count, trainCar.getPlacesEnds());
        statement.setLong(++count, trainCar.getTrainId());
        statement.setLong(++count, trainCar.getId());
    }

    private void setAttributeForSave(TrainCar trainCar, PreparedStatement statement) throws SQLException {
        int k = 0;
        statement.setLong(++k, trainCar.getNum());
        statement.setString(++k, valueOf(trainCar.getType()));
        statement.setLong(++k, trainCar.getPlacesBegins());
        statement.setLong(++k, trainCar.getPlacesEnds());
        statement.setLong(++k, trainCar.getTrainId());
    }

    private void setAttributeForSave(Ticket ticket, PreparedStatement statement) throws SQLException {
        int k = 0;
        statement.setLong(++k, ticket.getTrainCar().getId());
        statement.setLong(++k, ticket.getUser().getId());
        statement.setLong(++k, ticket.getBookedSeat());
        statement.setLong(++k, ticket.getTimetable().getId());
    }

    private TrainCar convertResultSetToTrainCar(ResultSet resultSet) throws SQLException {
        return TrainCar.builder()
                .id(resultSet.getLong("id"))
                .num(resultSet.getInt("num"))
                .type(TrainCar.Type.valueOf(resultSet.getString("type")))
                .trainId(resultSet.getLong("train_id"))
                .placesBegins(resultSet.getInt("places_begins"))
                .placesEnds(resultSet.getInt("places_ends"))
                .build();
    }
}
