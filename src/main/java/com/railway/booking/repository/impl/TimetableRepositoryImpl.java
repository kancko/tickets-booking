package com.railway.booking.repository.impl;

import com.railway.booking.entity.Timetable;
import com.railway.booking.repository.TimetableRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TimetableRepositoryImpl implements TimetableRepository {
    private static final String SELECT_ALL_TIMETABLE_BY_TRAIN_ID = "select * from timetable where train_id = ?";
    private static final String SELECT_ALL = "select * from timetable";
    private static final String SELECT_TIMETABLE_BY_ID = "select * from timetable where id = ?";
    private static final String INSERT_TIMETABLE = "insert into timetable (arrival_time, departure_time, train_id) values(?,?,?)";
    private static final String DELETE_TIMETABLE = "delete from timetable where id=?";
    private static final String UPDATE_TIMETABLE = "update timetable set arrival_time=?, departure_time=?, train_id=? where id=?";

    @Override
    public List<Timetable> getAllByTrainId(Connection connection, long trainId) throws SQLException {
        List<Timetable> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_TIMETABLE_BY_TRAIN_ID);
        statement.setLong(1, trainId);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToTimetable(resultSet));
        }
        return list;
    }

    @Override
    public List<Timetable> getAll(Connection connection) throws SQLException {
        List<Timetable> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        while (resultSet.next()) {
            list.add(convertResultSetToTimetable(resultSet));
        }
        return list;
    }

    @Override
    public Timetable getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_TIMETABLE_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToTimetable(resultSet) : null;
    }

    @Override
    public long save(Connection connection, Timetable entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TIMETABLE, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public Timetable delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_TIMETABLE);
        statement.setLong(1, id);
        statement.executeUpdate();
        return Timetable.builder().id(id).build();
    }

    @Override
    public Timetable update(Connection connection, Timetable entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TIMETABLE);
        setAttributeForUpdate(entity, statement);
        statement.executeUpdate();
        return entity;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForUpdate(Timetable timetable, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setTimestamp(++count, timetable.getArrivalTime());
        statement.setTimestamp(++count, timetable.getDepartureTime());
        statement.setLong(++count, timetable.getTrainId());
        statement.setLong(++count, timetable.getId());
    }

    private void setAttributeForSave(Timetable timetable, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setTimestamp(++count, timetable.getArrivalTime());
        statement.setTimestamp(++count, timetable.getDepartureTime());
        statement.setLong(++count, timetable.getTrainId());
    }

    private Timetable convertResultSetToTimetable(ResultSet resultSet) throws SQLException {
        return Timetable.builder()
                .id(resultSet.getLong("id"))
                .arrivalTime(resultSet.getTimestamp("arrival_time"))
                .departureTime(resultSet.getTimestamp("departure_time"))
                .build();
    }
}
