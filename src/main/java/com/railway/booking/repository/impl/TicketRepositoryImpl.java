package com.railway.booking.repository.impl;

import com.railway.booking.entity.*;
import com.railway.booking.repository.TicketRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.railway.booking.entity.Ticket.Type;

public class TicketRepositoryImpl implements TicketRepository {
    private static final String SELECT_BASE = "select cs.id cs_id, cs.title cs_title, ce.id ce_id, ce.title ce_title," +
            "u.id u_id, u.passport_series u_passport_series, u.full_name u_full_name, " +
            "u.username u_username, u.password u_password, u.role u_role, u.is_active u_is_active, " +
            "t.id t_id, t.train_number t_train_number, timet.departure_time timet_departure_time, " +
            "timet.arrival_time timet_arrival_time, t.cost t_cost, tcu.booked_seat tcu_booked_seat, " +
            "tc.type tc_type, tc.num tc_num, ti.id ti_id, ti.type ti_type, ti.discount_number ti_discount_number " +
            "from tickets ti " +
            "join timetable timet on timet.id = ti.timetable_id " +
            "join traincars_users tcu on tcu.id = ti.traincars_users_id " +
            "join traincars tc on tcu.traincar_id = tcu.traincar_id " +
            "join users u on u.id = tcu.user_id " +
            "join trains t on tc.train_id = t.id " +
            "join cities cs on cs.id = t.start_city " +
            "join cities ce on ce.id = t.end_city ";
    private static final String SELECT_ALL_TICKETS_BY_USER_ID = SELECT_BASE + "where tcu.user_id = ? and t.id = tcu.traincar_id";
    private static final String SELECT_TICKET_BY_ID = SELECT_BASE + "where ti.id = ?";
    private static final String INSERT_TICKET = "insert into tickets (traincars_users_id, timetable_id, discount_number, type) values(?,?,?,?)";
    private static final String DELETE_TICKET = "delete from tickets where id = ?";

    @Override
    public List<Ticket> getAll(Connection connection, long userId) throws SQLException {
        List<Ticket> list = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_TICKETS_BY_USER_ID);
        statement.setLong(1, userId);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            list.add(convertResultSetToTicket(resultSet));
        }
        return list;
    }

    @Override
    public Ticket getById(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_TICKET_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToTicket(resultSet) : null;
    }

    @Override
    public long save(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TICKET, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(entity, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public Ticket delete(Connection connection, long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_TICKET);
        statement.setLong(1, id);
        statement.executeUpdate();
        return Ticket.builder().id(id).build();
    }

    @Override
    public Ticket update(Connection connection, Ticket entity) {
        throw new UnsupportedOperationException();
    }

    private void setAttributeForSave(Ticket ticket, PreparedStatement statement) throws SQLException {
        int k = 0;
        statement.setLong(++k, ticket.getTrainCarUserId());
        statement.setLong(++k, ticket.getTimetable().getId());
        statement.setString(++k, ticket.getDiscountNumber());
        statement.setString(++k, ticket.getType().name());
    }

    private Ticket convertResultSetToTicket(ResultSet resultSet) throws SQLException {
        User user = getUserFromResultSet(resultSet);
        City endCity = getCityFromResultSet(resultSet, "ce");
        City startCity = getCityFromResultSet(resultSet, "cs");
        Train train = getTrainFromResultSet(resultSet, startCity, endCity);
        TrainCar trainCar = getTrainCarFromResultSet(resultSet);
        Timetable timetable = getTimetableFromResultSet(resultSet);
        return Ticket.builder()
                .user(user)
                .train(train)
                .trainCar(trainCar)
                .timetable(timetable)
                .id(resultSet.getLong("ti_id"))
                .bookedSeat(resultSet.getInt("tcu_booked_seat"))
                .type(Type.valueOf(resultSet.getString("ti_type")))
                .discountNumber(resultSet.getString("ti_discount_number"))
                .build();
    }

    private Timetable getTimetableFromResultSet(ResultSet resultSet) throws SQLException {
        return Timetable.builder()
                .departureTime(resultSet.getTimestamp("timet_departure_time"))
                .arrivalTime(resultSet.getTimestamp("timet_arrival_time"))
                .build();
    }

    private City getCityFromResultSet(ResultSet resultSet, String prefix) throws SQLException {
        return City.builder()
                .id(resultSet.getLong(prefix + "_id"))
                .title(resultSet.getString(prefix + "_title"))
                .build();
    }

    private TrainCar getTrainCarFromResultSet(ResultSet resultSet) throws SQLException {
        return TrainCar.builder()
                .num(resultSet.getInt("tc_num"))
                .type(TrainCar.Type.valueOf(resultSet.getString("tc_type")))
                .build();
    }

    private Train getTrainFromResultSet(ResultSet resultSet, City startCity, City endCity) throws SQLException {
        return Train.builder()
                .id(resultSet.getLong("t_id"))
                .trainNumber(resultSet.getString("t_train_number"))
                .cost(resultSet.getLong("t_cost"))
                .startCity(startCity)
                .endCity(endCity)
                .build();
    }

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        return User.builder()
                .id(resultSet.getLong("u_id"))
                .passportSeries(resultSet.getString("u_passport_series"))
                .fullName(resultSet.getString("u_full_name"))
                .username(resultSet.getString("u_username"))
                .password(resultSet.getString("u_password"))
                .build();
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }
}
