package com.railway.booking.transaction.impl;

import com.railway.booking.transaction.TransactionAction;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@RequiredArgsConstructor
public class TransactionManagerImpl implements TransactionManager {

    private final DataSource dataSource;

    @Override
    public <U> U execute(TransactionAction<U> transactionAction) {
        U result;
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            result = transactionAction.action(connection);
            connection.commit();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return result;
    }
}
