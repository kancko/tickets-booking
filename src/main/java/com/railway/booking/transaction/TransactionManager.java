package com.railway.booking.transaction;

public interface TransactionManager {

    <U> U execute(TransactionAction<U> transactionAction);
}
