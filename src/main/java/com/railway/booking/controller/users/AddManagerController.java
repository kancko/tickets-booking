package com.railway.booking.controller.users;

import com.railway.booking.common.ConverterUtils;
import com.railway.booking.constant.JspConstants;
import com.railway.booking.entity.User;
import com.railway.booking.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/manager-add")
public class AddManagerController extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig config) {
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(JspConstants.ADD_MANAGER_PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User user = ConverterUtils.convertRequestToUser(req);
        user.setRole(User.Role.MANAGER);
        userService.save(user);
        resp.sendRedirect("/admin");
    }
}
