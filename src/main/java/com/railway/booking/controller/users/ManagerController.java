package com.railway.booking.controller.users;

import com.railway.booking.constant.JspConstants;
import com.railway.booking.service.CityService;
import com.railway.booking.service.TrainService;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.lang.Long.parseLong;
import static org.apache.commons.lang3.StringUtils.defaultString;

@WebServlet("/manager")
public class ManagerController extends HttpServlet {

    private TrainService trainService;
    private CityService cityService;

    @Override
    public void init(ServletConfig config) {
        trainService = (TrainService) config.getServletContext().getAttribute(TrainService.class.toString());
        cityService = (CityService) config.getServletContext().getAttribute(CityService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String[] startDate = {""};
        Optional.ofNullable(req.getParameter("startDate")).filter(StringUtils::isNotEmpty).ifPresent(s ->
                startDate[0] = Timestamp.valueOf(LocalDateTime.parse(s)).toString());
        final String[] endDate = {""};
        Optional.ofNullable(req.getParameter("endDate")).filter(StringUtils::isNotEmpty).ifPresent(s ->
                endDate[0] = Timestamp.valueOf(LocalDateTime.parse(s)).toString());
        String startCity = defaultString(req.getParameter("startCity"));
        String endCity = defaultString(req.getParameter("endCity"));
        req.setAttribute("trains", trainService.getTrains(startDate[0], endDate[0], startCity, endCity));
        req.setAttribute("cities", cityService.getCities());
        req.getParameterMap().forEach((k, v) -> req.setAttribute(k, v[0]));
        req.getRequestDispatcher(JspConstants.MANAGER_HOME).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getParameter("action").equals("city")) {
            cityService.delete(parseLong(req.getParameter("cityId")));
        } else if (req.getParameter("action").equals("train")) {
            trainService.delete(parseLong(req.getParameter("trainId")));
        }
        resp.sendRedirect("/manager");
    }
}
