package com.railway.booking.controller.users;

import com.railway.booking.constant.JspConstants;
import com.railway.booking.entity.Ticket;
import com.railway.booking.entity.User;
import com.railway.booking.service.TicketService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user")
public class UserController extends HttpServlet {

    private TicketService ticketService;

    @Override
    public void init(ServletConfig config) {
        ticketService = (TicketService) config.getServletContext().getAttribute(TicketService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        List<Ticket> tickets = ticketService.getAllTickets(user.getId());
        req.setAttribute("tickets", tickets);
        req.getRequestDispatcher(JspConstants.USER_HOME).forward(req, resp);
    }
}
