package com.railway.booking.controller.trains;

import com.railway.booking.entity.TrainCar;
import com.railway.booking.service.TrainCarService;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

@WebServlet("/manager/train-car")
public class TrainCarController extends HttpServlet {

    private TrainCarService trainCarService;

    @Override
    public void init(ServletConfig config) {
        trainCarService = (TrainCarService) config.getServletContext().getAttribute(TrainCarService.class.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        long trainId = parseLong(req.getParameter("trainId"));
        trainCarService.save(getTrainCarFromRequest(req, trainId));
        resp.sendRedirect("/manager/train/details?trainId=" + trainId);
    }

    private TrainCar getTrainCarFromRequest(HttpServletRequest req, long trainId) {
        return TrainCar.builder()
                .num(parseInt(req.getParameter("num")))
                .type(TrainCar.Type.valueOf(req.getParameter("type")))
                .placesBegins(parseInt(req.getParameter("placesBegins")))
                .placesEnds(parseInt(req.getParameter("placesEnds")))
                .trainId(trainId)
                .build();
    }
}
