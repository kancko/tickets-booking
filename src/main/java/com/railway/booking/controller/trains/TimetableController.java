package com.railway.booking.controller.trains;

import com.railway.booking.entity.Timetable;
import com.railway.booking.service.TimetableService;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.lang.Long.parseLong;

@WebServlet("/manager/timetable")
public class TimetableController extends HttpServlet {

    private TimetableService timetableService;

    @Override
    public void init(ServletConfig config) {
        timetableService = (TimetableService) config.getServletContext().getAttribute(TimetableService.class.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        long trainId = parseLong(req.getParameter("trainId"));
        timetableService.save(getTimetableFromRequest(req, trainId));
        resp.sendRedirect("/manager/train/details?trainId=" + trainId);
    }

    private Timetable getTimetableFromRequest(HttpServletRequest req, long trainId) {
        return Timetable.builder()
                .arrivalTime(getDateTimeFromRequest(req, "startDate"))
                .departureTime(getDateTimeFromRequest(req, "endDate"))
                .trainId(trainId)
                .build();
    }

    private Timestamp getDateTimeFromRequest(HttpServletRequest req, String param) {
        return Optional.ofNullable(req.getParameter(param))
                .map(LocalDateTime::parse)
                .map(Timestamp::valueOf)
                .get();
    }
}
