package com.railway.booking.controller.trains;

import com.railway.booking.constant.JspConstants;
import com.railway.booking.entity.City;
import com.railway.booking.service.CityService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/manager/city")
public class CityController extends HttpServlet {

    private CityService cityService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("cities", cityService.getCities());
        req.getRequestDispatcher(JspConstants.ADD_CITY_PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        cityService.save(City.builder().title(req.getParameter("title")).build());
        resp.sendRedirect("/manager/city");
    }
}
