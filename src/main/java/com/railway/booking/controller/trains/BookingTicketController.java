package com.railway.booking.controller.trains;

import com.google.common.collect.ImmutableMap;
import com.railway.booking.constant.JspConstants;
import com.railway.booking.entity.*;
import com.railway.booking.service.TicketService;
import com.railway.booking.service.TimetableService;
import com.railway.booking.service.TrainCarService;
import com.railway.booking.service.TrainService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.lang.Long.parseLong;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@WebServlet("/booking-ticket")
public class BookingTicketController extends HttpServlet {

    private TrainService trainService;
    private TicketService ticketService;
    private TrainCarService trainCarService;
    private TimetableService timetableService;

    private Map<User.Role, String> redirect = ImmutableMap.of(User.Role.CONSUMER, "/user",
            User.Role.MANAGER, "/manager",
            User.Role.ADMIN, "/admin");

    @Override
    public void init(ServletConfig config) {
        trainService = (TrainService) config.getServletContext().getAttribute(TrainService.class.toString());
        ticketService = (TicketService) config.getServletContext().getAttribute(TicketService.class.toString());
        trainCarService = (TrainCarService) config.getServletContext().getAttribute(TrainCarService.class.toString());
        timetableService = (TimetableService) config.getServletContext().getAttribute(TimetableService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long trainId = parseLong(req.getParameter("trainId"));
        long trainCarId = parseLong(req.getParameter("trainCar"));
        long timetableId = parseLong(req.getParameter("time"));
        List<Integer> bookedSeats = trainService.getBookedSeats(trainId);
        TrainCar trainCar = trainCarService.getById(trainCarId);
        trainCar.setSeats(initSeats(trainCar, bookedSeats));
        req.setAttribute("trainCar", trainCar);
        req.setAttribute("train", trainService.getById(trainId));
        req.setAttribute("timetable", timetableService.getById(timetableId));
        req.getRequestDispatcher(JspConstants.BOOKING_TICKET_PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ticketService.save(Ticket.builder()
                .discountNumber(req.getParameter("discount"))
                .type(isNotEmpty(req.getParameter("type")) ? Ticket.Type.DISCOUNT : Ticket.Type.FULL)
                .bookedSeat(Integer.parseInt(req.getParameter("seat")))
                .train(Train.builder().id(Long.parseLong(req.getParameter("trainId"))).build())
                .trainCar(TrainCar.builder().id(parseLong(req.getParameter("trainCarId"))).build())
                .user(User.builder().id(((User) req.getSession().getAttribute("user")).getId()).build())
                .timetable(Timetable.builder().id(Integer.parseInt(req.getParameter("timetableId"))).build())
                .build()
        );
        resp.sendRedirect(redirect.get(((User) req.getSession().getAttribute("user")).getRole()));
    }

    private List<Integer> initSeats(TrainCar trainCar, List<Integer> bookedSeats) {
        List<Integer> seats = new ArrayList<>();
        for (int i = trainCar.getPlacesBegins(); i <= trainCar.getPlacesEnds(); i++) {
            seats.add(i);
        }
        seats.removeAll(bookedSeats);
        return seats;
    }
}
