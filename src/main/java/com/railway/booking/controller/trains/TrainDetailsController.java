package com.railway.booking.controller.trains;

import com.railway.booking.constant.JspConstants;
import com.railway.booking.service.TimetableService;
import com.railway.booking.service.TrainCarService;
import com.railway.booking.service.TrainService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Long.parseLong;

@WebServlet("/train/details")
public class TrainDetailsController extends HttpServlet {

    private TrainService trainService;
    private TrainCarService trainCarService;
    private TimetableService timetableService;

    @Override
    public void init(ServletConfig config) {
        trainService = (TrainService) config.getServletContext().getAttribute(TrainService.class.toString());
        trainCarService = (TrainCarService) config.getServletContext().getAttribute(TrainCarService.class.toString());
        timetableService = (TimetableService) config.getServletContext().getAttribute(TimetableService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long trainId = parseLong(req.getParameter("trainId"));
        req.setAttribute("train", trainService.getById(trainId));
        req.setAttribute("trainCars", trainCarService.getAllByTrainId(trainId));
        req.setAttribute("timetable", timetableService.getAllByTrainId(trainId));
        req.setAttribute("discount", req.getParameter("discount"));
        req.getRequestDispatcher(JspConstants.TRAINS_DETAILS).forward(req, resp);
    }
}
