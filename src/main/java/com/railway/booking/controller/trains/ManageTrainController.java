package com.railway.booking.controller.trains;

import com.railway.booking.constant.JspConstants;
import com.railway.booking.entity.City;
import com.railway.booking.entity.Train;
import com.railway.booking.service.CityService;
import com.railway.booking.service.TrainService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Long.parseLong;

@WebServlet("/manager/train")
public class ManageTrainController extends HttpServlet {

    private CityService cityService;
    private TrainService trainService;

    @Override
    public void init(ServletConfig config) {
        cityService = (CityService) config.getServletContext().getAttribute(CityService.class.toString());
        trainService = (TrainService) config.getServletContext().getAttribute(TrainService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("cities", cityService.getCities());
        req.getRequestDispatcher(JspConstants.TRAINS_MANAGE_PAGE).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        long trainId = trainService.save(getTrainFromRequest(req));
        resp.sendRedirect("/manager/train/details?trainId=" + trainId);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) {
        Train train = getTrainFromRequest(req);
        trainService.update(train);
    }

    private Train getTrainFromRequest(HttpServletRequest req) {
        long cost = parseLong(req.getParameter("cost"));
        String trainNumber = req.getParameter("trainNumber");
        City endCity = City.builder().id(Long.parseLong(req.getParameter("endCity"))).build();
        City startCity = City.builder().id(Long.parseLong(req.getParameter("startCity"))).build();
        return Train.builder()
                .endCity(endCity)
                .startCity(startCity)
                .trainNumber(trainNumber)
                .cost(cost)
                .build();
    }
}
