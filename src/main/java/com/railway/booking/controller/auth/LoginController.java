package com.railway.booking.controller.auth;

import com.railway.booking.entity.User;
import com.railway.booking.exception.ValidationException;
import com.railway.booking.service.UserService;
import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

import static com.railway.booking.constant.JspConstants.USER_LOGIN_JSP;
import static com.railway.booking.entity.User.Role.*;
import static com.railway.booking.exception.ValidationException.ValidationEnum.BAD_CREDENTIAL;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

@WebServlet("/login")
public class LoginController extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(USER_LOGIN_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("username");
        String password = sha256Hex(request.getParameter("password"));
        User user = userService.getUserByUsernameAndPassword(email, password);
        if (Objects.nonNull(user)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            if (CONSUMER.equals(user.getRole())) {
                response.sendRedirect("/user");
            } else if (MANAGER.equals(user.getRole())) {
                response.sendRedirect("/manager");
            } else if (ADMIN.equals(user.getRole())){
                response.sendRedirect("/admin");
            }
        } else {
            ValidationException.builder().put(BAD_CREDENTIAL, "Bad credential").throwIfErrorExists();
        }
    }

}
