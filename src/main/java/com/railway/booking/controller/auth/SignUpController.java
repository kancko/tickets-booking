package com.railway.booking.controller.auth;

import com.railway.booking.entity.User;
import com.railway.booking.exception.ValidationException;
import com.railway.booking.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.railway.booking.common.ConverterUtils.convertRequestToUser;
import static com.railway.booking.constant.JspConstants.SIGN_UP;
import static com.railway.booking.exception.ValidationException.ValidationEnum.USERNAME_EXISTS;

@WebServlet("/sign-up")
public class SignUpController extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig config) {
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(SIGN_UP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = convertRequestToUser(request);
        if (userService.existsByUserName(user.getUsername())) {
            ValidationException.builder().put(USERNAME_EXISTS, "Username already exist").throwIfErrorExists();
        }
        user.setId(userService.save(user));
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        response.sendRedirect("/user/user-home");

    }
}
