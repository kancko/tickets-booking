package com.railway.booking.filter;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import java.io.IOException;

@Slf4j
public class ExceptionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        log.debug("init exception filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (Exception ex) {
            log.error("error", ex);
            throw ex;
        }
    }

    @Override
    public void destroy() {
        log.debug("destroy exception filter");
    }
}
