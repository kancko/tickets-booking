package com.railway.booking.filter;

import com.railway.booking.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

public class SecurityFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(SecurityFilter.class);

    private static Pattern userPattern = Pattern.compile("/user.*");
    private static Pattern adminPattern = Pattern.compile("/admin.*?|/manager/.{1,}?");
    private static Pattern managerPattern = Pattern.compile("/manager.*");

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        LOG.debug("SecurityFilter");
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpResp = (HttpServletResponse) response;
        boolean check = checkForAccess(httpReq, httpResp, adminPattern, User.Role.ADMIN);
        check &= checkForAccess(httpReq, httpResp, managerPattern, User.Role.MANAGER);
        check &= checkForAccess(httpReq, httpResp, userPattern, User.Role.CONSUMER);
        if (check) {
            filterChain.doFilter(request, response);
        }
        request.setAttribute("errorPage", (httpResp).getStatus());
    }

    @Override
    public void destroy() {

    }

    private boolean checkForAccess(HttpServletRequest request, HttpServletResponse response, Pattern pattern, User.Role... roles) throws IOException {
        Integer error = null;
        if (pattern.matcher(request.getServletPath()).matches()) {
            User user = getUser(request);
            if (Objects.nonNull(user) && user.isActive()) {
                for (User.Role role : roles) {
                    if (!role.equals(user.getRole())) {
                        error = HttpServletResponse.SC_FORBIDDEN;
                        break;
                    }
                }
            } else {
                error = HttpServletResponse.SC_UNAUTHORIZED;
            }
        }

        if (Objects.isNull(error)) {
            return true;
        } else {
            response.sendError(error);
            return false;
        }
    }

    private User getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (Objects.nonNull(session)) {
            return (User) session.getAttribute("user");
        }
        return null;
    }
}
