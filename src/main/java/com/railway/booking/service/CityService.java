package com.railway.booking.service;

import com.railway.booking.entity.City;
import com.railway.booking.entity.User;

import java.util.List;

public interface CityService extends EntityService<City> {

    List<City> getCities();
}
