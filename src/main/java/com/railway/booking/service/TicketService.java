package com.railway.booking.service;

import com.railway.booking.entity.Ticket;

import java.util.List;

public interface TicketService extends EntityService<Ticket> {

    List<Ticket> getAllTickets(long userId);
}
