package com.railway.booking.service;

import com.railway.booking.entity.Train;

import java.util.List;

public interface TrainService extends EntityService<Train> {

    List<Train> getTrains(String startDate, String endDate, String startCity, String endCity);

    List<Integer> getBookedSeats(long trainId);
}
