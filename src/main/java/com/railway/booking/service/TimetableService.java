package com.railway.booking.service;

import com.railway.booking.entity.Timetable;

import java.util.List;

public interface TimetableService extends EntityService<Timetable> {

    List<Timetable> getAll();

    List<Timetable> getAllByTrainId(long trainId);
}
