package com.railway.booking.service;

import com.railway.booking.entity.User;

import java.util.List;

public interface UserService extends EntityService<User> {

    User getUserByUsernameAndPassword(String username, String password);

    boolean existsByUserName(String username);

    List<User> getAll();

    long block(long userId, boolean active);
}
