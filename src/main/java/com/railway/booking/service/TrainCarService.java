package com.railway.booking.service;

import com.railway.booking.entity.Ticket;
import com.railway.booking.entity.TrainCar;

import java.util.List;

public interface TrainCarService extends EntityService<TrainCar> {

    List<TrainCar> getAllByTrainId(long trainId);

    List<TrainCar> getAll();

    long saveTrainCarUser(Ticket ticket);

    List<Integer> getBookedSeats(long trainId);
}
