package com.railway.booking.service;

public interface EntityService<T> {

    T getById(long id);

    long save(T entity);

    T delete(long id);

    T update(T entity);
}
