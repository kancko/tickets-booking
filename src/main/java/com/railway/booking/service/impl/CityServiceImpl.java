package com.railway.booking.service.impl;

import com.railway.booking.entity.City;
import com.railway.booking.repository.CityRepository;
import com.railway.booking.service.CityService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;
    private final TransactionManager transactionManager;

    @Override
    public List<City> getCities() {
        return transactionManager.execute(cityRepository::getCities);
    }

    @Override
    public City getById(long id) {
        return transactionManager.execute(c -> cityRepository.getById(c, id));
    }

    @Override
    public long save(City entity) {
        return transactionManager.execute(c -> cityRepository.save(c, entity));
    }

    @Override
    public City delete(long id) {
        return transactionManager.execute(c -> cityRepository.delete(c, id));
    }

    @Override
    public City update(City entity) {
        return transactionManager.execute(c -> cityRepository.update(c, entity));
    }
}
