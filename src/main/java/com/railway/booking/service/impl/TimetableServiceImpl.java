package com.railway.booking.service.impl;

import com.railway.booking.entity.Timetable;
import com.railway.booking.repository.TimetableRepository;
import com.railway.booking.service.TimetableService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TimetableServiceImpl implements TimetableService {

    private final TimetableRepository timetableRepository;
    private final TransactionManager transactionManager;

    @Override
    public List<Timetable> getAllByTrainId(long trainId) {
        return transactionManager.execute(c -> timetableRepository.getAllByTrainId(c, trainId));
    }

    @Override
    public List<Timetable> getAll() {
        return transactionManager.execute(timetableRepository::getAll);
    }

    @Override
    public Timetable getById(long id) {
        return transactionManager.execute(c -> timetableRepository.getById(c, id));
    }

    @Override
    public long save(Timetable entity) {
        return transactionManager.execute(c -> timetableRepository.save(c, entity));
    }

    @Override
    public Timetable delete(long id) {
        return transactionManager.execute(c -> timetableRepository.delete(c, id));
    }

    @Override
    public Timetable update(Timetable entity) {
        return transactionManager.execute(c -> timetableRepository.update(c, entity));
    }
}
