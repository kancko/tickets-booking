package com.railway.booking.service.impl;

import com.railway.booking.entity.User;
import com.railway.booking.repository.UserRepository;
import com.railway.booking.service.UserService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final TransactionManager transactionManager;
    private final UserRepository userRepository;

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        return transactionManager.execute(c -> userRepository.getUserByUsernameAndPassword(c, username, password));
    }

    @Override
    public boolean existsByUserName(String username) {
        return transactionManager.execute(c -> userRepository.existsByUserName(c, username));
    }

    @Override
    public List<User> getAll() {
        return transactionManager.execute(userRepository::getAll);
    }

    @Override
    public long block(long userId, boolean active) {
        return transactionManager.execute(c -> userRepository.block(c, userId, active));
    }

    @Override
    public User getById(long id) {
        return transactionManager.execute(c -> userRepository.getById(c, id));
    }

    @Override
    public long save(User entity) {
        return transactionManager.execute(c -> userRepository.save(c, entity));
    }

    @Override
    public User delete(long id) {
        return transactionManager.execute(c -> userRepository.delete(c, id));
    }

    @Override
    public User update(User entity) {
        return transactionManager.execute(c -> userRepository.update(c, entity));
    }
}
