package com.railway.booking.service.impl;

import com.railway.booking.entity.Train;
import com.railway.booking.repository.TrainRepository;
import com.railway.booking.service.TimetableService;
import com.railway.booking.service.TrainCarService;
import com.railway.booking.service.TrainService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TrainServiceImpl implements TrainService {

    private final TrainCarService trainCarService;
    private final TrainRepository trainRepository;
    private final TimetableService timetableService;
    private final TransactionManager transactionManager;

    @Override
    public List<Train> getTrains(String startDate, String endDate, String startCity, String endCity) {
        return transactionManager.execute(c -> trainRepository.getTrains(c, startDate, endDate, startCity, endCity));
    }

    @Override
    public Train getById(long id) {
        Train train = transactionManager.execute(c -> trainRepository.getById(c, id));
        train.setTimetable(timetableService.getAllByTrainId(id));
        return train;
    }

    @Override
    public List<Integer> getBookedSeats(long trainId) {
        return trainCarService.getBookedSeats(trainId);
    }

    @Override
    public long save(Train entity) {
        return transactionManager.execute(c -> trainRepository.save(c, entity));
    }

    @Override
    public Train delete(long id) {
        return transactionManager.execute(c -> trainRepository.delete(c, id));
    }

    @Override
    public Train update(Train entity) {
        return transactionManager.execute(c -> trainRepository.update(c, entity));
    }
}
