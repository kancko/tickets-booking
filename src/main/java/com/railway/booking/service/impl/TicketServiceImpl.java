package com.railway.booking.service.impl;

import com.railway.booking.entity.Ticket;
import com.railway.booking.repository.TicketRepository;
import com.railway.booking.service.TicketService;
import com.railway.booking.service.TrainCarService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TransactionManager transactionManager;
    private final TicketRepository ticketRepository;
    private final TrainCarService trainCarService;

    @Override
    public List<Ticket> getAllTickets(long userId) {
        return transactionManager.execute(c -> ticketRepository.getAll(c, userId));
    }

    @Override
    public Ticket getById(long id) {
        return transactionManager.execute(c -> ticketRepository.getById(c, id));
    }

    @Override
    public long save(Ticket entity) {
        entity.setTrainCarUserId(trainCarService.saveTrainCarUser(entity));
        return transactionManager.execute(c -> ticketRepository.save(c, entity));
    }

    @Override
    public Ticket delete(long id) {
        return transactionManager.execute(c -> ticketRepository.delete(c, id));
    }

    @Override
    public Ticket update(Ticket entity) {
        throw new UnsupportedOperationException();
    }
}
