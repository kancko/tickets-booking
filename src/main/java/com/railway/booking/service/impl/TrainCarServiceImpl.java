package com.railway.booking.service.impl;

import com.railway.booking.entity.Ticket;
import com.railway.booking.entity.TrainCar;
import com.railway.booking.entity.User;
import com.railway.booking.repository.TrainCarRepository;
import com.railway.booking.service.TrainCarService;
import com.railway.booking.transaction.TransactionManager;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class TrainCarServiceImpl implements TrainCarService {

    private final TrainCarRepository trainCarRepository;
    private final TransactionManager transactionManager;

    @Override
    public List<TrainCar> getAllByTrainId(long trainId) {
        return transactionManager.execute(c -> trainCarRepository.getAllByTrainId(c, trainId));
    }

    @Override
    public List<TrainCar> getAll() {
        return transactionManager.execute(trainCarRepository::getAll);
    }

    @Override
    public List<Integer> getBookedSeats(long trainId) {
        return transactionManager.execute(c -> trainCarRepository.getBookedSeats(c, trainId));
    }

    @Override
    public TrainCar getById(long id) {
        return transactionManager.execute(c -> trainCarRepository.getById(c, id));
    }

    @Override
    public long saveTrainCarUser(Ticket ticket) {
        return transactionManager.execute(c -> trainCarRepository.saveTrainCarUser(c, ticket));
    }

    @Override
    public long save(TrainCar entity) {
        return transactionManager.execute(c -> trainCarRepository.save(c, entity));
    }

    @Override
    public TrainCar delete(long id) {
        return transactionManager.execute(c -> trainCarRepository.delete(c, id));
    }

    @Override
    public TrainCar update(TrainCar entity) {
        return transactionManager.execute(c -> trainCarRepository.update(c, entity));
    }
}
