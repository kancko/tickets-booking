package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class User extends Entity {

    private String username;
    private String password;
    private String fullName;
    private String passportSeries;
    private boolean isActive;
    private Role role;

    public enum Role {
        CONSUMER, MANAGER, ADMIN
    }

    @Builder
    public User(long id, String username, String password, String fullName, String passportSeries, boolean isActive, Role role) {
        super(id);
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.passportSeries = passportSeries;
        this.isActive = isActive;
        this.role = role;
    }
}
