package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Ticket extends Entity {

    private Type type;
    private User user;
    private Train train;
    private int bookedSeat;
    private TrainCar trainCar;
    private long trainCarUserId;
    private Timetable timetable;
    private String discountNumber;

    public enum Type {
        DISCOUNT, FULL
    }

    @Builder
    public Ticket(long id, Type type, User user, Train train, String discountNumber,
                  TrainCar trainCar, Timetable timetable, int bookedSeat) {
        super(id);
        this.type = type;
        this.user = user;
        this.train = train;
        this.trainCar = trainCar;
        this.timetable = timetable;
        this.bookedSeat = bookedSeat;
        this.discountNumber = discountNumber;
    }
}
