package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Timetable extends Entity {

    private Timestamp departureTime;
    private Timestamp arrivalTime;
    private long trainId;

    @Builder
    public Timetable(long id, Timestamp departureTime, Timestamp arrivalTime, long trainId) {
        super(id);
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.trainId = trainId;
    }
}
