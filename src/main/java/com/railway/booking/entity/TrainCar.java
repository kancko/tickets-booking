package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class TrainCar extends Entity {

    private int placesBegins;
    private int placesEnds;
    private int num;
    private long trainId;
    private Type type;
    private List<Integer> seats;

    public static enum Type {
        COUPE, ECONOM
    }

    @Builder
    public TrainCar(long id, int placesBegins, int placesEnds, int num, long trainId, Type type) {
        super(id);
        this.placesBegins = placesBegins;
        this.placesEnds = placesEnds;
        this.num = num;
        this.trainId = trainId;
        this.type = type;
    }
}
