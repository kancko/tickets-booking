package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Train extends Entity {

    private long cost;
    private City endCity;
    private City startCity;
    private String trainNumber;
    private List<Timetable> timetable;


    @Builder
    public Train(long id, long cost, City endCity, City startCity, String trainNumber, List<Timetable> timetable) {
        super(id);
        this.cost = cost;
        this.endCity = endCity;
        this.startCity = startCity;
        this.trainNumber = trainNumber;
        this.timetable = timetable;
    }
}
