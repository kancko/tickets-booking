package com.railway.booking.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class City extends Entity {

    private String title;

    @Builder
    public City(long id, String title) {
        super(id);
        this.title = title;
    }
}
