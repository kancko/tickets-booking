<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Edit Test"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="editTest">
    <a href="/admin/admin-home" id="back"><i class="fas fa-arrow-circle-left"></i></a>
    <div id="testInfo">
        <p><fmt:message key="testedit"/></p>
        <form action="/admin/edit-test" method="post">
            <input type="hidden" name="id" value="${test.id}">
            <input type="text" name="name" value="${test.name}" placeholder="<fmt:message key="testname"/>" required><br>
            <input type="text" name="subject" value="${test.subject.name}" placeholder="<fmt:message key="subject"/>" required><br>
            <select name="complexity" size="1">
                <option value="LOW"
                <c:if test="${test.complexity eq 'LOW'}">selected</c:if>
                >Low</option>
                <option value="MEDIUM"
                <c:if test="${test.complexity eq 'MEDIUM'}">selected</c:if>
                >Medium</option>
                <option value="HARD"
                <c:if test="${test.complexity eq 'HARD'}">selected</c:if>
                >Hard</option>
            </select><br>
            <input type="number" name="duration_minutes" value="${test.durationMinutes}" min="1" max="20"
                   placeholder="<fmt:message key="duration"/>" required><br>
            <input type="submit" id="deleteUpdateTest" name="submit" value="<fmt:message key="update"/>">
        </form>
        <form action="/admin/delete-test" method="post">
            <input type="hidden" name="id" value="${test.id}">
            <input type="submit" id="deleteUpdateTest" name="submit" value="<fmt:message key="delete"/>">
        </form>
        <c:if test="${not empty errors}">
            <fmt:message key="${errors['deleteError']}"/>
        </c:if>
    </div>

    <div id="questionInfo">
        <c:forEach var="question" items="${test.questions}">
            <div id="question">
                <p><fmt:message key="questedit"/></p>
                <form action="/admin/edit-question" method="post" id="editQuestion">
                    <input type="hidden" name="testId" value="${test.id}">
                    <input type="hidden" name="id" value="${question.id}">
                    <input type="text" name="name" value="${question.name}" placeholder="<fmt:message key="questname"/>" required>
                    <input type="text" name="description" value="${question.description}" placeholder="<fmt:message key="desc"/>"
                           required>
                    <input type="submit" name="submit" value="&#10004" id="accept">
                </form>
                <form action="/admin/delete-question" method="post" id="deleteQuestion">
                    <input type="hidden" name="testId" value="${test.id}">
                    <input type="hidden" name="id" value="${question.id}">
                    <input type="submit" name="submit" value="&#10008" id="decline">
                </form>
            </div>

            <c:if test="${not empty question.answers}"> <p id="pEditAnswer"><fmt:message key="answedit"/></p> </c:if>
            <c:forEach var="answer" items="${question.answers}">
                <div id="answer">
                    <form action="/admin/edit-answer" method="post" id="editAnswer">
                        <input type="hidden" name="id" value="${answer.id}">
                        <input type="hidden" name="questionId" value="${question.id}">
                        <input type="hidden" name="testId" value="${test.id}">
                        <input type="text" name="name" value="${answer.name}" placeholder="<fmt:message key="answname"/>" required/>
                        <div class="check-material">
                            <input id="${answer.id}" type="checkbox" name="answers" value="true"
                            <c:if test="${answer.correct eq 'true'}">checked</c:if>
                            >
                            <label for="${answer.id}"></label>
                        </div>
                        <input type="submit" name="submit" value="&#10004" id="accept">
                    </form>
                    <form action="/admin/delete-answer" method="post" id="deleteAnswer">
                        <input type="hidden" name="testId" value="${test.id}">
                        <input type="hidden" name="questionId" value="${question.id}">
                        <input type="hidden" name="id" value="${answer.id}">
                        <input type="submit" name="submit" value="&#10008" id="decline">
                    </form>
                </div>
            </c:forEach>

            <p id="pEditAnswer"><fmt:message key="newansw"/></p>
            <form action="/admin/create-answer" method="post" id="answerForm">
                <input type="hidden" name="testId" value="${test.id}">
                <input type="hidden" name="questionId" value="${question.id}">
                <input type="text" name="name" placeholder="<fmt:message key="answname"/>" required/>
                <div class="check-material">
                    <input id="label-${question.id}" type="checkbox" name="isCorrect" value="true">
                    <label for="label-${question.id}"></label>
                </div>
                <input type="submit" name="submit" value="&#10004" id="accept">
            </form>

        </c:forEach>

        <p><fmt:message key="newquest"/></p>
        <form action="/admin/create-question" method="post" style="text-align: start;">
            <input type="hidden" name="testId" value="${test.id}">
            <input type="text" name="name" placeholder="<fmt:message key="questname"/>" required>
            <input type="text" name="description" placeholder="<fmt:message key="desc"/>" required>
            <input type="submit" name="submit" value="&#10004" id="accept">
        </form>
    </div>
</div>
</body>
</html>