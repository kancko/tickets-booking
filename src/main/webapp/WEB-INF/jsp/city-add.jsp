<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="User Homepage"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

    <form action="/manager/city" method="post">
        <input type="text" hidden="Enter city name"><br>
        <input type="submit" value="Save">
    </form>

    <table>
        <c:forEach var="city" items="${cities}">
            <tr>
                <td>${city.title}</td>
                <td>
                    <form method="post" action="/manager/city">
                        <input type="${city.id}">
                        <input type="submit" value="Delete">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
