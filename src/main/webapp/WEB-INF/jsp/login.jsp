<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="title" value="Sign-Up/Login Form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="la" tagdir="/WEB-INF/tags"%>

<div id="wrapper" style="margin: 100;text-align: -webkit-center;">
    <form action="/login" method="post" style="width: fit-content;">
        <div id="login" style="border: solid 1px;padding: 10 40;border-radius: 7px;">
            <label>Login</label>
            <p><input type="text" name="username" placeholder="<fmt:message key="email"/>" required></p>
            <p><input type="password" name="password" placeholder="<fmt:message key="password"/>"
                                                            required></p>
            <p><input type="submit" name="submit" value="<fmt:message key="signin"/>"></p>
            <p><fmt:message key="noacc"/>? &nbsp;&nbsp;<a href="/sign-up"><fmt:message key="signup"/></a><span class="fontawesome-arrow-right"></span></p>
        </div>
    </form>
    <p>
        <c:forEach var="error" items="${errors}">
            ${error.value}
        </c:forEach>
    </p>
</div>
</body>
</html>