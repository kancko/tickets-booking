<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Create Test form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div style="margin: 100;">

    <div style="text-align: center;">
        <label><b>Ticket Booking</b></label>
        <br><br>
    </div>

    <form method="post" action="/booking-ticket">
        <input type="hidden" name="discount" value="${discount}">
        <input type="hidden" name="trainId" value="${train.id}">
        <input type="hidden" name="trainCarId" value="${train.id}">
        <input type="hidden" name="timetableId" value="${timetable.id}">

        <b>Train info</b><br>
        Number: ${train.trainNumber} Cost: ${train.cost}<br>
        Start city: ${train.startCity.title} End City: ${train.endCity.title}<br><br>

        <b>Train car</b><br>
        <div>
            Car number: ${trainCar.num} Car type: ${trainCar.type}<br>
            Places begins: ${trainCar.placesBegins} Places ends: ${trainCar.placesEnds}
        </div><br>

        <b>Timetable</b><br>
        <div>
            Arrival Time: ${timetable.arrivalTime}<br>
            Departure Time: ${timetable.departureTime}<br>
        </div><br>

        <b>Choose seat</b>
        <select name="seat">
            <c:forEach var="seat" items="${trainCar.seats}">
                <option value="${seat}">${seat}</option>
            </c:forEach>
        </select>
        <input type="submit">
    </form>
</div>
</body>
</html>