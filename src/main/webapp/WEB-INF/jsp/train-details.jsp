<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Create Test form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="wrapper" style="margin: 100;">
    <div style="text-align: center;">
        <label><b>Ticket Booking</b></label>
        <br><br>
    </div>

    <form method="get" action="/booking-ticket">
        <div  id="trainInfo" style="display: inline-block;text-align: left;vertical-align:top;">
        <input type="hidden" name="trainId" value="${param.trainId}">

        <b>Train info</b><br>
        Number: ${train.trainNumber} Cost: ${train.cost}<br>
        Start city: ${train.startCity.title} End City: ${train.endCity.title}<br><br>

        <b>Discount Number</b><br>
            <input type="text" name="discount" placeholder="Enter here"><br>
        </div>
        <div id="ticket" style="display: inline-block;text-align:left;margin-left: 20px;">
        <b>Train cars</b><br>
        <select name="trainCar">
            <c:forEach var="trainCar" items="${trainCars}">
                <option value="${trainCar.id}">
                    <div>
                        Car number: ${trainCar.num} Car type: ${trainCar.type}<br>
                        Places begins: ${trainCar.placesBegins} Places ends: ${trainCar.placesEnds}
                    </div>
                </option>
            </c:forEach>
        </select><br><br><br>
            <b>Timetable</b><br>
        <select name="time">
            <c:forEach var="time" items="${timetable}">
                <option value="${time.id}">
                    <div>
                        Arrival: ${time.arrivalTime} <br>Departure: ${time.departureTime}<br>
                    </div>
                </option>
            </c:forEach>
        </select><br><br>
            <input type="submit" style="float: right;">
        </div>
    </form>
</div>
</body>
</html>