<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="User Homepage"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
    <div id="wrapper" style="margin: 100;text-align: center;">
        <form action="/trains" method="get">
            <label>Find your train</label><br><br>
            <input type="datetime-local" name="startDate"
            <c:if test="${not empty startDate}">
                value="${startDate}"
            </c:if>
            >
            <input type="datetime-local" name="endDate"
            <c:if test="${not empty endDate}">
                value="${endDate}"
            </c:if>
            >
            <select name="startCity">
                <c:forEach var="city" items="${cities}">
                    <option value="${city.id}"
                    <c:if test="${startCity.matches(city.id)}">selected</c:if>
                    >${city.title}</option>
                </c:forEach>
            </select>
            <select name="endCity">
                <c:forEach var="city" items="${cities}">
                    <option value="${city.id}"
                    <c:if test="${endCity.matches(city.id)}">selected</c:if>
                    >${city.title}</option>
                </c:forEach>
            </select>
            <input type="submit">
        </form>

        <table style="width: -webkit-fill-available;">
            <tr>
                <td>Train number</td>
                <td>Arrival city</td>
                <td>Departure City</td>
                <td>Cost</td>
            </tr>
            <c:forEach var="train" items="${trains}">
                <tr onclick="window.location.href='/train/details?trainId=${train.id}'; return false" class="link">
                    <td>${train.trainNumber}</td>
                    <td>${train.startCity.title}</td>
                    <td>${train.endCity.title}</td>
                    <td>${train.cost}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
