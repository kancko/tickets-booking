<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Sign-Up/Login Form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="la" tagdir="/WEB-INF/tags"%>


<div id="wrapper" style="margin: 100;text-align: -webkit-center;">
    <form action="sign-up" method="post" style="width: fit-content;">
        <div id="login" style="border: solid 1px;padding: 10 40;border-radius: 7px;">
            <label><a href="/login" id="back"><span class="fas fa-arrow-circle-left"></span></a> Sing Up</label>
            <p><input type="text" name="username" placeholder="username" required></p>
            <p><input type="text" name="fullname" placeholder="fullname" required></p>
            <p><input type="text" name="passportSeries" placeholder="passport series" required></p>
            <p><input type="password" name="password" maxlength="20"
                      placeholder="password" required pattern="[0-9a-zA-Z]{6,20}"></p>
            <p><input type="password" name="rePassword" maxlength="20"
                      placeholder="repeat password" required pattern="[0-9a-zA-Z]{6,20}"></p>
            <p><input type="submit" value="Sign Up"></p>
        </div>
    </form>
    <p>
        <c:forEach var="error" items="${errors}">
            ${error.value}
        </c:forEach>
    </p>
</div>
</body>
</html>
