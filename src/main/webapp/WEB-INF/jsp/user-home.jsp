<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Admin Panel"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="wrapper" style="margin: 30;text-align: center;">
    <div class="d1">
        <div>
            <div id="title_user">
                <span class="fas fa-user"></span>&nbsp;&nbsp;&nbsp; Username: ${user.username}<br>
                <a href="/logout"><span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;&nbsp; logout</a>
            </div>
            <hr>
            <span>Full name: ${user.fullName}</span><br>
            <span>Account type: ${user.role}</span><br>
        </div>
        <hr>
        <div>
            <ul style="margin-left: -20px">
                <li><a href="/trains">Search trains</a></li>
            </ul>
        </div>
    </div>
    <div class="d2">
        <div class="table_content" style="padding: 10 30">
            <span>Booked tickets</span><br><br>
            <table>
            <c:forEach var="ticket" items="${tickets}">
                <tr onclick="window.location.href='/student/test-pass?id=${test.id}'; return false" id="link">
                    <td>
                        <div>
                            Full Name: ${ticket.user.fullName}<br>
                            Passport: ${ticket.user.passportSeries}<br>
                        </div>
                    </td>
                    <td>
                        <div>
                            Train: ${ticket.train.trainNumber}<br>
                            Arrival city: ${ticket.train.startCity.title}<br>
                            Departure city: ${ticket.train.endCity.title}<br>
                            Cost: ${ticket.train.cost}<br>
                        </div>
                    </td>
                    <td>
                        <div>
                            Ticket type: ${ticket.type}<br>
                            Train car num: ${ticket.trainCar.num}<br>
                            Train car type: ${ticket.trainCar.type}<br>
                            Discount num: ${ticket.discountNumber}<br>
                        </div>
                    </td>
                    <td>
                        <div>
                            <fmt:formatDate value="${ticket.timetable.departureTime}" pattern="dd.MM.yyyy HH:mm"/><br>
                            <fmt:formatDate value="${ticket.timetable.arrivalTime}" pattern="dd.MM.yyyy HH:mm"/><br>
                            ${ticket.bookedSeat}
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </table>
        </div>
    </div>
</div>

</body>
</html>
