<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Admin Panel"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="wrapper">
    <div class="d1">
        <div>
            <div id="title_user">
                <span class="fas fa-user"></span>&nbsp;&nbsp;&nbsp; Username: ${user.username}<br>
                <a href="/logout"><span class="fas fa-sign-out-alt"></span>&nbsp;&nbsp;&nbsp; logout</a>
            </div>
            <hr>
            <span>Full name: ${user.fullName}</span><br>
            <span>Account type: ${user.role}</span><br>
        </div>
        <hr>
        <div>
            <ul style="margin-left: -20px">
                <li><a href="/manager/city">Add city</a></li>
                <li><a href="/manager/train">Add train</a></li>
                <li><a href="/trains">Search trains</a></li>
            </ul>
        </div>
    </div>
    <div class="d2">
        <div class="table_content" style="text-align: left; padding: 10 30">
            <form action="/trains" method="get">
                Arrival date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="datetime-local" name="startDate"
                <c:if test="${not empty startDate}">
                    value="${startDate}"
                </c:if>
                ><br><br>
                Departure date: &nbsp;&nbsp;&nbsp;
                <input type="datetime-local" name="endDate"
                <c:if test="${not empty endDate}">
                    value="${endDate}"
                </c:if>
                ><br><br>
                Arrival city: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <select name="startCity">
                    <c:forEach var="city" items="${cities}">
                        <option value="${city.id}"
                        <c:if test="${startCity.matches(city.id)}">selected</c:if>
                        >${city.title}</option>
                    </c:forEach>
                </select><br><br>
                Departure city: &nbsp;&nbsp;&nbsp;
                <select name="endCity">
                    <c:forEach var="city" items="${cities}">
                        <option value="${city.id}"
                        <c:if test="${endCity.matches(city.id)}">selected</c:if>
                        >${city.title}</option>
                    </c:forEach>
                </select>
                <input type="submit" value="Search">
            </form>
            <hr>
            <div style="display: inline-block; margin-right: 20px;">
                <table>
                <caption>Trains</caption>
                <c:forEach var="train" items="${trains}">
                    <tr>
                        <td>${train.trainNumber}</td>
                        <td>${train.startCity.title}</td>
                        <td>${train.endCity.title}</td>
                        <td>${train.cost}</td>
                        <td>
                            <form method="post" action="/manager">
                                <input type="hidden" name="action" value="train">
                                <input type="hidden" name="trainId" value="${train.id}">
                                <input type="submit" value="Delete">
                            </form>
                        </td>
                        <td>
                            <form method="get" action="/manager/train/details">
                                <input type="hidden" name="trainId" value="${train.id}">
                                <input type="submit" value="Edit">
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            </div>
            <div style="display: inline-block; vertical-align: top;">
                <table>
            <caption>Cities</caption>
            <c:forEach var="city" items="${cities}">
                <tr>
                    <td>${city.title}</td>
                    <td>
                        <form method="post" action="/manager">
                            <input type="hidden" name="action" value="city">
                            <input type="hidden" name="cityId" value="${train.id}">
                            <input type="submit" value="Delete">
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
            </div>
        </div>
    </div>
    <div class="d2">

    </div>
</div>

</body>
</html>
