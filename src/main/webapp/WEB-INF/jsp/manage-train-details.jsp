<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Create Test form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div style="margin: 20px; position: fixed; top: 20px; left: 20px;"><a href="/manager">Go home</a></div>
<div id="wrapper" style="margin: 50 100;">
    <span>Manage train details</span>
    <br><br>
    <div>
        <div style="display: inline-block; text-align: left; vertical-align: top;">
            <form action="/manager/timetable" method="post">
                <input type="hidden" name="trainId" value="${train.id}">
                Arrival date:
                <input type="datetime-local" name="startDate">
                <br><br>
                Departure date:
                <input type="datetime-local" name="endDate">
                <br>
                <br>
                <input type="submit">
            </form>
        </div>
        <div style="display: inline-block; text-align: left;">
            <form action="/manager/train-car" method="post">
                <input type="hidden" name="trainId" value="${train.id}">
                Train car number:
                <input type="text" name="num" placeholder="Train car number">
                <br><br>
                Train car type:
                <select name="type">
                    <c:forEach var="type" items="${types}">
                        <option value="${type}">${type}</option>
                    </c:forEach>
                </select>
                <br><br>
                Places starts with:
                <input type="number" name="placesBegins" min="1" max="50">
                <br><br>
                Places ends:
                <input type="number" name="placesEnds" min="1" max="50">
                <br><br>
                <input type="submit">
            </form>
        </div>
        <br>
        <div style="text-align: left; display: inline-block; width: -webkit-fill-available; padding: 0 20%;">
        <b>Train info</b><br>
        Number: ${train.trainNumber} Start city: ${train.startCity.title} End City: ${train.endCity.title} Cost: ${train.cost}
        <br><br>
        <b>Timetable</b><br>
        <c:forEach var="timetable" items="${train.timetable}">
            Arrival Time: ${timetable.arrivalTime} Departure Time: ${timetable.departureTime}<br>
        </c:forEach>
        <br>
        <b>Train cars</b><br>
        <c:forEach var="trainCar" items="${trainCars}">
            Places begins: ${trainCar.placesBegins} Places ends: ${trainCar.placesEnds} Car number: ${trainCar.num} Car type: ${trainCar.type}<br>
        </c:forEach>
    </div>
    </div>
</div>
</body>
</html>