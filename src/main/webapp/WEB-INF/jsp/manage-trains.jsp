<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Create Test form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div style="margin: 20px; position: fixed; top: 20px; left: 20px;"><a href="/manager">Go home</a></div>
<div id="wrapper" style="margin: 50 100;">
    <span><b>Add new train</b></span>
    <br><br>
    <div>
        <form action="/manager/train" method="post" style="display: inline-block; text-align: left;">
        <input type="text" name="cost" placeholder="Cost">
        <input type="text" name="trainNumber" placeholder="Train number">
        <br><br>
        Arrival city:
        <select name="startCity">
            <c:forEach var="city" items="${cities}">
                <option value="${city.id}">${city.title}</option>
            </c:forEach>
        </select>
        <br><br>
        Departure city:
        <select name="endCity">
            <c:forEach var="city" items="${cities}">
                <option value="${city.id}">${city.title}</option>
            </c:forEach>
        </select>
        <br><br>
        <input type="submit" value="Add">
    </form>
    </div>
</div>
</body>
</html>